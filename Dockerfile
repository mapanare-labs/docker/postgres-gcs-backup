FROM alpine:edge

RUN apk add --update \
  bash \
  postgresql16 \
  curl \
  python3 \
  py3-pip \
  py3-cffi \
  && pip install --upgrade pip --break-system-packages \
  && apk add --virtual build-deps \
  gcc \
  libffi-dev \
  python3-dev \
  linux-headers \
  musl-dev \
  openssl-dev \
  && pip install gsutil --break-system-packages \
  && apk del build-deps \
  && rm -rf /var/cache/apk/*

ADD . /postgres-gcs-backup

WORKDIR /postgres-gcs-backup

RUN chmod +x /postgres-gcs-backup/backup.sh

ENTRYPOINT ["/postgres-gcs-backup/backup.sh"]